# -*- coding: utf-8 -*-
"""
BBB Plugin.

Reference doc:
https://docs.bigbluebutton.org/admin/monitoring.html
https://docs.bigbluebutton.org/dev/api.html
https://gist.github.com/bwurst/7f94e0392c75d273a08d1e686182fc5e
https://github.com/dbrgn/collectd-python-plugins
https://blog.dbrgn.ch/2017/3/10/write-a-collectd-python-plugin/
"""


import collectd
import subprocess
import re
import hashlib
import urllib2
import xml.etree.ElementTree as ET
from contextlib import closing

def read():

	##params
	api_url = None
	api_shared = None
	api_call='getMeetings'
	api_query=''

	##get url and secret from bbb-conf
	#doc: https://gist.github.com/bwurst/7f94e0392c75d273a08d1e686182fc5e
	tmp = subprocess.check_output(['/usr/bin/bbb-conf', '--secret'])

	for line in tmp.splitlines():
		m = re.search('URL: (?P<URL>.*/bigbluebutton/)', line)
		if m:
			api_url = m.group('URL') + 'api/'
			continue
		m = re.search('Secret: (?P<secret>.*)$', line)
		if m:
			api_shared = m.group('secret')

	##checksum
	string=api_call+api_query+api_shared
	checksum=hashlib.sha1(string.encode()).hexdigest()

	##resource_url
	if (len(api_query)>0):
		resource_url=api_url+api_call+'?'+api_query+'&checksum='+checksum
	else:
		resource_url=api_url+api_call+'?'+'checksum='+checksum
		#print (string)
		#print (checksum)
		#print (resource_url)

	##stats
	with closing(urllib2.urlopen(resource_url)) as response:
		html = response.read()
		tree = ET.fromstring(html)
		meetings = tree.find('meetings')
		rooms = 0
		participants = 0
		listenerparticipants = 0
		voiceparticipants = 0
		videoparticipants = 0
		audiostreams = 0
		videostreams = 0
		for meeting in meetings.findall('meeting'):
			rooms += 1
			participants += int(meeting.find('participantCount').text)
			listenerparticipants += int(meeting.find('listenerCount').text)
			voiceparticipants += int(meeting.find('voiceParticipantCount').text)
			videoparticipants += int(meeting.find('videoCount').text)
			videostreams += (videoparticipants*(participants-1))
			audiostreams = participants
			#print(rooms, participants,listenerparticipants,voiceparticipants,videoparticipants,videostreams,audiostreams)





	##dispatch values
	v_rooms = collectd.Values(plugin='bbb', type='bbb_rooms', type_instance='rooms')
	v_rooms.dispatch(values=[rooms])
	v_users = collectd.Values(plugin='bbb', type='bbb_users', type_instance='all')
	v_users.dispatch(values=[participants])
	v_users = collectd.Values(plugin='bbb', type='bbb_users', type_instance='listeners')
	v_users.dispatch(values=[listenerparticipants])
	v_users = collectd.Values(plugin='bbb', type='bbb_users', type_instance='voice')
	v_users.dispatch(values=[voiceparticipants])
	v_users = collectd.Values(plugin='bbb', type='bbb_users', type_instance='video')
	v_users.dispatch(values=[videoparticipants])
	v_streams = collectd.Values(plugin='bbb', type='bbb_streams', type_instance='video')
	v_streams.dispatch(values=[videostreams])
	v_streams = collectd.Values(plugin='bbb', type='bbb_streams', type_instance='audio')
	v_streams.dispatch(values=[audiostreams])

collectd.register_read(read)
