# Collectd plugins


### BigBlueButton

Python plugin for rooms, streams and users monitorization.

To install clone this repo in /opt
~~~
cd /opt
git clone https://gitlab.com/MaadiX/collectd.git
~~~

Then, activate the plugin (python 2 version). In /etc/collectd/collectd.conf
~~~
<Plugin python>
    ModulePath "/opt/collectd" 
    Import "bbb" 
</Plugin>
~~~

There's a python3 version of the plugin also.


